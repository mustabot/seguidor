#include <Mustabot2016Lib.h>  //Mustabot Library
#include <ColorPal.h>         //ColorPal Library

#include <SFE_MicroOLED.h>  // MicroOLED Library

//////////////////////////
// MicroOLED Definition //
//////////////////////////
#define PIN_RESET 48  // Connect RST to pin 9
#define PIN_DC    49  // Connect DC to pin 8
#define PIN_CS    53 // Connect CS to pin 10
#define DC_JUMPER 0

//////////////////////////
// Delaracion MicroOLED //
//////////////////////////
MicroOLED oled(PIN_RESET, PIN_DC, PIN_CS); // SPI declaration

/////////////////////////////////
// Declaracion Rangos ColorPal //
/////////////////////////////////
#define luzIzq 32
#define luzCen 33
#define luzDer 34

#define rNeA 160
#define rNeB 0

#define rVeA 00
#define rVeB 0

#define rBaA 160
#define rBaB 1000

///////////////////////////
// Declaracion ColorPals //
///////////////////////////
ColorPal colorIzq(6, Serial1);
ColorPal colorDer(8, Serial2);

int val = 0;
int valColorIzq = 0;
int valColorDer = 0;
int valLuzIzq = 0;
int valLuzDer = 0;
int valLuzCen = 0;
int valIzq = 0;
int valDer = 0;
int e = 0;
int e_log[2];
unsigned long vez = 0;
int zona = 0;
int barrido = 0;
int tiempo = 0;
unsigned long inicio;

void setup() {
  // put your setup code here, to run once:
  noInterrupts();
  colorIzq.begin();     //Inicia ColorPalIzq
  colorDer.begin();     //Inicia ColorPalDer


  Serial.begin(9600);   //Serial para PC
  Serial1.begin(7200);  //Serial para ColorPalIzq
  Serial2.begin(7200);  //Serial para ColorPalDer

  pinMode(A11, OUTPUT);
  pinMode(A12, OUTPUT);
  pinMode(A13, OUTPUT);
  pinMode(A14, OUTPUT);
  pinMode(A15, OUTPUT);


  oled.begin();

  interrupts();

  inicio = millis();
}

void loop() {
  // put your main code here, to run repeatedly:

  if ( millis() - inicio < 10000) {
    if (e != e_log[0]) {
      e_log[1] = e_log[0];
      e_log[0] = e;
    }

    if (val != 0) {
      zona = 0;
      barrido = 0;
    }

    leerSensores();       //Leer Sensores

    digitalWrite(A11, !valLuzIzq);
    digitalWrite(A12, !valIzq);
    digitalWrite(A13, !valLuzCen);
    digitalWrite(A14, !valDer);
    digitalWrite(A15, !valLuzDer);

    error();


    serial();

    print_oled();

    moverse();


    //avanzar(0);
  }

}

void error() {

  ////////////////////////
  // Seleccion de Error //
  ////////////////////////

  switch (val) {
    case 100: //B-B-N-B-B
      e = 0;
      break;

    case 110: //B-B-N-N-B
      e = 1;
      break;

    case 10: //B-B-B-N-B
      e = 2;
      break;

    case 11: //B-B-B-N-N
      e = 3;
      break;

    case 1: //B-B-B-B-N
      e = 4;
      break;

    //----------------
    case 1100: //B-N-N-B-B
      e = -1;
      break;

    case 1000: //B-N-B-B-B
      e = -2;
      break;

    case 11000: //N-N-B-B-B
      e = -3;
      break;

    case 10000: //B-B-B-B-B
      e = -4;
      break;

    //////////////////////
    // Casos Especiales //
    //////////////////////

    case 0:
      if (zona == 0) {
        avanzar(255, 255);
        delay(500);
        zona++;
        break;
      }

      if ((barrido == 0) || (barrido == 3)) {
        while ((tiempo > 200) && (val == 0)) {
          avanzar(0, 200);
          tiempo++;
          leerSensores();
        }
        tiempo = 0;
        barrido++;
      }//if barrido

      if ((barrido == 1) || (barrido == 2)) {
        while ((tiempo > 200) && (val == 0)) {
          avanzar(200, 0);
          tiempo++;
          leerSensores();
        }
        tiempo = 0;
        barrido++;
      }
      break;


    default:
      avanzar(0);
      e = -100;
      break;
  }

}

void moverse() {

  ////////////////////////////
  // Movimiento según error //
  ////////////////////////////

  switch (e) {

    case 0:
      avanzar(255);
      break;

    /////////////////////////
    // Giros hacia derecha //
    /////////////////////////

    case 1:
      avanzar(255, 100);
      break;

    case 2:
      avanzar(255, 50);
      break;

    case 3:
      avanzar(255, -100);
      break;

    case 4:
      avanzar(255, -255);
      break;

    ///////////////////////////
    // Giros hacia izquierda //
    ///////////////////////////

    case -1:
      avanzar(100, 255);
      break;

    case -2:
      avanzar(50, 255);
      break;

    case -3:
      avanzar(-100, 255);
      break;

    case -4:
      avanzar(-255, 255);
      break;

    case -100:
      avanzar(0);
      break;
  }
}

void retroceder() {
  switch (e) {

    case 0:
      avanzar(-255);
      break;

    /////////////////////////
    // Giros hacia derecha //
    /////////////////////////

    case 1:
      avanzar(-255, -200);
      break;

    case 2:
      avanzar(-255, -100);
      break;

    case 3:
      avanzar(-255, 100);
      break;

    case 4:
      avanzar(-255, 255);
      break;

    ///////////////////////////
    // Giros hacia izquierda //
    ///////////////////////////

    case -1:
      avanzar(-200, -255);
      break;

    case -2:
      avanzar(-100, -255);
      break;

    case -3:
      avanzar(100, -255);
      break;

    case -4:
      avanzar(255, -255);
      break;

    case -100:
      avanzar(0);
      break;

  }
}

void serial() {
  ////////////////////////////////
  // Impresion de valores en PC //
  ////////////////////////////////

  Serial.print(valLuzIzq);
  Serial.print("\t");
  Serial.print(valColorIzq);
  Serial.print("\t");
  Serial.print(valLuzCen);
  Serial.print("\t");
  Serial.print(valColorDer);
  Serial.print("\t");
  Serial.print(valLuzDer);
  Serial.print("\t");
  Serial.print(val);
  Serial.print("\t");
  Serial.print(e);
  Serial.print("\t");
  Serial.print(e_log[0]);
  Serial.print("\t");
  Serial.print(e_log[1]);
  Serial.print("\t");
  Serial.print(inicio);
  Serial.print("\t");
  Serial.println(millis());


  if (vez % 10 == 0)
    Serial.println("LuzIzq:\tColorIzq:LuzCen:ColorDer:LuzDer:Val:\tE:\tE[0]:\tE[1]:");

  vez++;

}

void print_oled() {
  oled.setCursor(0, 0);
  oled.clear(PAGE);
  oled.print(val);
  oled.print("\nLI: ");
  oled.print(valLuzIzq);
  oled.print("\nCI: ");
  oled.print(valColorIzq);
  oled.print("\nLC: ");
  oled.print(valLuzCen);
  oled.print("\nCD: ");
  oled.print(valColorDer);
  oled.print("\nLD: ");
  oled.print(valLuzDer);
  oled.display();
}

void leerSensores() {
  valLuzIzq = digitalRead(luzIzq);
  valLuzCen = digitalRead(luzCen);
  valLuzDer = digitalRead(luzDer);
  valIzq = 0;
  valDer = 0;

  if ( valColorIzq >= rNeB && valColorIzq <= rNeA) valIzq = 1;
  else if ( valColorIzq >= rBaB && valColorIzq <= rBaA ) valIzq = 0;
  else if ( valColorIzq >= rVeB && valColorIzq <= rVeA ) valIzq = 2;

  if ( valColorDer >= rNeB && valColorDer <= rNeA) valDer = 1;
  else if ( valColorDer >= rBaB && valColorDer <= rBaA ) valDer = 0;
  else if ( valColorDer >= rVeB && valColorDer <= rVeA ) valDer = 2;

  val = (valLuzIzq * 10000) + (valIzq * 1000) + (valLuzCen * 100) + (valDer * 10) + valLuzDer;

}

void serialEvent1() {
  char t;
  int val = colorIzq.read();
  if (val >= 0) valColorIzq = val;
  else if (val == -1) while (Serial1.available() > 0) t = Serial1.read();
  else;
}

void serialEvent2() {
  char t;
  int val = colorDer.read();
  if (val >= 0) valColorDer = val;
  else if (val == -1) while (Serial2.available() > 0) t = Serial2.read();
  else;
}

